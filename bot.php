<?php

require_once 'Messenger.php';
require_once 'MessengerHelper.php';
require_once 'db.php';

ini_set('error_log', 'bot_logs.log');

use \messenger\Messenger;
use \messenger\MessengerHelper;

Messenger::db_config($db);
MessengerHelper::db_config($db);

$accessToken = Messenger::get_access_token();
$hubVerifyToken = Messenger::get_hub_verify_token();

/* validate verify token needed for setting up web hook */ 
if (isset($_GET['hub_verify_token'])) { 
    if ($_GET['hub_verify_token'] === $hubVerifyToken) {
        echo $_GET['hub_challenge'];
        return;
    } else {
        echo 'Invalid Verify Token';
        return;
    }
}

/* receive and send messages */
$input = json_decode(file_get_contents('php://input'), true);

if (isset($input['entry'][0]['messaging'][0]['sender']['id'])) {

    $senderID = $input['entry'][0]['messaging'][0]['sender']['id']; //sender facebook id
    $message = $input['entry'][0]['messaging'][0]['message']['text'] ?? null; //text that user sent
    $url = 'https://graph.facebook.com/v2.6/me/messages?access_token='.$accessToken;
    
    $typingOn = MessengerHelper::sender_action($senderID, 'typing_on'); // sending typing_on
    $markSeen = MessengerHelper::sender_action($senderID, 'mark_seen'); // sending mark_seen
    $replyMessage = MessengerHelper::get_answer($senderID, strtolower($message)); // sending message to tagging
    
    if (!empty($message)) {
        Messenger::reply_to_sender($url, $markSeen, array('Content-Type: application/json')); // reply to sender
        Messenger::reply_to_sender($url, $typingOn, array('Content-Type: application/json')); // reply to sender
        Messenger::reply_to_sender($url, $replyMessage, array('Content-Type: application/json')); // reply to sender
    }
}

?>