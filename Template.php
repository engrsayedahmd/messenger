<?php

/*
* DYNAMIC MESSENGER BOT
* @author Md.Sayed Ahammed
* @e-mail : engrsayedahmd@gmail.com
* @version betaV1.0
* @date 2018-09-14 14.00.02
* @Copyright : all rights reserve to author
*/

namespace messenger;

class Template {
    
  //send basic text message
    public static function basic_text_mesg($senderID, $reply) {
        
        $jsonData = '{
        "recipient":{
            "id":"' . $senderID . '"
            },
            "message":{
                "text":"' . $reply . '"
            }
        }';
        
        return $jsonData;
    }  
    
    // send quick reply
    public static function quick_replies($senderID, $text, $quickReplies) {
        
        $encoded = array();
        
        foreach($quickReplies as $quickReply)
        {
            array_push($encoded,json_encode($quickReply));
        }
        
        $encodedQuickReplies = implode(',',$encoded);
        
        $jsonData= '{
            "recipient":{
                "id":"'.$senderID.'"
              },
              "message":{
                "text": "'.$text.'",
                "quick_replies":[
                  '.$encodedQuickReplies.'
                ]
              }
        }';
    
        return $jsonData;

    }
    
    public static function sender_actions($senderID, $action_type) {
         
         switch($action_type) {
             case 'typing_on':
               $jsonData = '{
                  "recipient":{
                    "id":"'.$senderID.'"
                  },
                  "sender_action":"typing_on"
                }';
                return $jsonData;
                break;
                
             case 'mark_seen':
                 $jsonData = '{
                  "recipient":{
                    "id":"'.$senderID.'"
                  },
                  "sender_action":"mark_seen"
                }';
                return $jsonData;
                break;
         }
    }

}
