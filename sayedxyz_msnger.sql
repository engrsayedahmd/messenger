-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 23, 2018 at 03:08 AM
-- Server version: 5.7.23-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sayedxyz_msnger`
--

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(10) NOT NULL,
  `access_token` longtext NOT NULL,
  `hub_verify_token` longtext NOT NULL,
  `greeting_text` text NOT NULL,
  `greeting_text_status` int(10) NOT NULL,
  `get_start_status` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `access_token`, `hub_verify_token`, `greeting_text`, `greeting_text_status`, `get_start_status`) VALUES
(1, 'EAADx19OfUMIBACZBqA63rFGEJ67EYJK9rkZBkn0zORleJCA6l3uu5FKg6gNb6dEa88iV8lufUM1mkYgKzUyfkbnVZCM1aZCvKy4zvbbgv36QcQzTtiVg2tEkZBEnQBHLvycEqjTKwjf4FV3seZAJDuETV7bvfdSeMIS6LZBJVppYh2xPZCKrB55y', 'build_own_facebook', 'Hi, {{user_first_name}} Welcome to Adda Bot!', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quick_reply`
--

CREATE TABLE `quick_reply` (
  `id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `payload` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quick_reply`
--

INSERT INTO `quick_reply` (`id`, `message`, `text`, `content_type`, `title`, `payload`) VALUES
(1, 'greetings', 'Greetings', 'text', 'Hi', 'hi'),
(3, 'greetings', 'Greetings', 'text', 'Hello', 'hello');

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE `suggestions` (
  `id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `content_type` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `payload` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggestions`
--

INSERT INTO `suggestions` (`id`, `query`, `text`, `content_type`, `title`, `payload`) VALUES
(1, 'hi', 'Suggestions ', 'text', 'Hello', 'hello'),
(2, 'hi', 'Suggestions', 'text', 'How are you ?', 'how are you');

-- --------------------------------------------------------

--
-- Table structure for table `tagging`
--

CREATE TABLE `tagging` (
  `id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagging`
--

INSERT INTO `tagging` (`id`, `message`, `tag`) VALUES
(1, 'hi', 'message'),
(2, 'greetings', 'quick_reply'),
(3, 'hello', 'message'),
(5, 'how are you', 'message'),
(8, 'hlw', 'message');

-- --------------------------------------------------------

--
-- Table structure for table `text_messages`
--

CREATE TABLE `text_messages` (
  `id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `reply` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `text_messages`
--

INSERT INTO `text_messages` (`id`, `message`, `reply`) VALUES
(1, 'hi', 'Hello'),
(2, 'hello', 'Hi, There!'),
(7, 'how are you', 'I am fine. And you ?'),
(10, 'hlw', 'Hi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_reply`
--
ALTER TABLE `quick_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagging`
--
ALTER TABLE `tagging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `text_messages`
--
ALTER TABLE `text_messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quick_reply`
--
ALTER TABLE `quick_reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tagging`
--
ALTER TABLE `tagging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `text_messages`
--
ALTER TABLE `text_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
