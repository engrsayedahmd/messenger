<?php

/*
* DYNAMIC MESSENGER BOT
* @author Md.Sayed Ahammed
* @e-mail : engrsayedahmd@gmail.com
* @version betaV1.0
* @date 2018-09-14 14.00.02
* @Copyright : all rights reserve to author
*/

namespace messenger;

class Messenger {
    
    // hold database configuration 
    private static $db;
    
    // config database
    public static function db_config($db){
        self::$db = $db;
    }
    
    // get access token
    public static function get_access_token() {
        
        $sql = "SELECT access_token FROM `config`";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(2);
        return $result['access_token'];
    }
    
    // get access token
    public static function get_hub_verify_token() {
        
        $sql = "SELECT hub_verify_token FROM `config`";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(2);
        return $result['hub_verify_token'];
    }
    
    // curl post method
    public static function reply_to_sender($url, $postFields, $headers) {
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    } 
}
