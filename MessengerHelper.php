<?php

/*
* DYNAMIC MESSENGER BOT
* @author Md.Sayed Ahammed
* @e-mail : engrsayedahmd@gmail.com
* @version betaV1.0
* @date 2018-09-14 14.00.02
* @Copyright : all rights reserve to author
*/

namespace messenger;

require_once 'Template.php';

use \messenger\Template;

class MessengerHelper {
    
   // hold database configuration 
    private static $db;
    
    // config database
    public static function db_config($db){
        self::$db = $db;
    }
    
   //tagging sender message
    public static function get_answer($senderID, $message) {
        
        $sql = "SELECT * FROM `tagging` WHERE message = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($message));
        $result = $stmt->fetch(2);
        $tag = $result['tag'];
        
        if($tag === 'message') {
           return self::text_message($senderID, $message);
        } elseif($tag === 'quick_reply'){
            return self::get_quick_replies($senderID, $message);
        } else {
            return Template::basic_text_mesg($senderID, 'Train me');
        }
    }
    
    // Displaying a Sender Action
    public static function sender_action($senderID, $action_type) {
        
        return Template::sender_actions($senderID, $action_type);
    }
    
    // get text message
    public static function text_message($senderID, $message) {
        
        $sql = "SELECT * FROM `text_messages` WHERE message = ?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($message));
        $result = $stmt->fetch(2);
        $reply = $result['reply'];
        
        return Template::basic_text_mesg($senderID, $reply); 
    }
    
    //get quick message
    public static function get_quick_replies($senderID, $message) {
        
        $quick_replies = array();
        $text = "";
        
        $sql = "SELECT * FROM quick_reply WHERE message=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($message));
        $res = $stmt->fetchAll(2);
        
        foreach($res as $quick_reply)
        {
            $text = $quick_reply['text'];
            
            $tmp = array(
                'content_type' => $quick_reply['content_type'],
                'title' => $quick_reply['title'],
                'payload' => $quick_reply['payload'],
                );
                
            array_push($quick_replies, $tmp);
            
        }
        
        return Template::quick_replies($senderID, $text, $quick_replies);
    
    }
}


